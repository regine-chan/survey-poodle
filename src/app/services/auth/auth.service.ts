import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  register(user): Promise<any> {
    return this.http.post("https://service-poodle.herokuapp.com/v1/api/users/register", {
      user: { ...user }
    }).toPromise();
  }
}
