import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user = {
    username: '',
    password: ''
  }

  isLoading: boolean = false;

  registerError: string;

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
  }

  async onRegisterClicked() {
    try {

      this.isLoading = true;

      const result: any = await this.auth.register(this.user);


    } catch (error) {
      this.registerError = error.error.error;
    } finally {
      this.isLoading = false;
    }

  }

}
